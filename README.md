# First Steps
Update the system with :
```bash
sudo apt update && sudo apt dist-upgrade
```
# Table des matières
1. [Raspberry Setup](00.raspi%20setup/README.md)
2. [Portainer](01.portainer/README.md)
3. [Maria DB](02.mariadb/README.md)
4. [Java Machine](03.java-machine/README.md)
5. [Elasticsearch](05.elasticsearch/README.md)

