# Install the Maria DB
## Edit the docker-compose.yml

```yaml
version: '2.4'
services:

  mariadb:
    image: tobi312/rpi-mariadb:10.11-debian
    container_name: mariadb
    restart: unless-stopped
    volumes:
      - ~/mariadb-data:/var/lib/mysql:rw
    environment:
      TZ: Europe/Paris
      MARIADB_ROOT_PASSWORD: cartel2000
      MARIADB_DATABASE: mydb
      MARIADB_USER: user
      MARIADB_PASSWORD: cartel2000
      MARIADB_MYSQL_LOCALHOST_USER: true  # need for healthcheck.sh
    ports:
      - 3306:3306
    healthcheck:
      test: ["CMD", "/usr/local/bin/healthcheck.sh", "--su-mysql", "--connect", "--innodb_initialized"]
      interval: 60s
      timeout: 5s
      retries: 5
```
Create the docker container
```bash
docker-compose -f docker-compose.yaml up -d
```


