# Create the java docker machine
```dockerfile
# Example of custom Java runtime using jlink in a multi-stage container build
FROM debian:bookworm-slim

LABEL version="1.0"
LABEL description="Install any package using SDKMan"

RUN adduser --shell /bin/sh appuser
WORKDIR /home/appuser

ENV SDKMAN_DIR /home/appuser/.sdkman
ENV JAVA_VERSION 17.0.10-tem

RUN apt-get update && apt-get install -y zip curl
RUN rm /bin/sh && ln -s /bin/bash /bin/sh
RUN curl -s "https://get.sdkman.io" | bash
RUN chmod a+x "$SDKMAN_DIR/bin/sdkman-init.sh"

RUN set -x \
   && echo "sdkman_auto_answer=true" > $SDKMAN_DIR/etc/config \
   && echo "sdkman_auto_selfupdate=false" >> $SDKMAN_DIR/etc/config \
   && echo "sdkman_insecure_ssl=false" >> $SDKMAN_DIR/etc/config

WORKDIR $SDKMAN_DIR
RUN [[ -s "$SDKMAN_DIR/bin/sdkman-init.sh" ]] && source "$SDKMAN_DIR/bin/sdkman-init.sh" && exec "$@"

RUN source /root/.bashrc
RUN source "$SDKMAN_DIR/bin/sdkman-init.sh" && sdk install java $JAVA_VERSION
```
Build it with command line
```bash
 sudo docker build -t mgoulene/java-machine-17 .
```