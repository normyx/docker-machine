#!/bin/sh

source "$SDKMAN_DIR/bin/sdkman-init.sh"

echo "The application will start in ${JHIPSTER_SLEEP}s..." && sleep ${JHIPSTER_SLEEP}
exec java ${JAVA_OPTS} -Xms1000m -jar "${HOME}/myaccount3.jar" "$@"