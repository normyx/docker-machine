# Installing MyAccount V3

## Build the image
```bash
docker build -t mgoulene/myaccount3 . 
```
## Deploy the container
```bash
docker-compose -f myaccount.yml up -d
```
## Modifying the parameter
Modify connection parameter in the docker compose file `myaccount.yml`
```yaml
        environment:
            - _JAVA_OPTIONS=
            - SPRING_PROFILES_ACTIVE=prod,no-liquibase
            - SPRING_DATASOURCE_URL=jdbc:mysql://myaccount.dbms.home:3306/myaccount?useUnicode=true&characterEncoding=utf8&useSSL=false
            - SPRING_ELASTICSEARCH_URIS=http://myaccount.elasticsearch.home:9200
            - JHIPSTER_SLEEP=10 # gives time for the database to boot before the application

```