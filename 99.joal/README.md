docker run -it --privileged mgoulene/joal bash

docker run --name="joal" --privileged -p 9100:9100 -v /home/mathieu/docker_data/joal_data:/data mgoulene/joal up -d

sudo docker build -t mgoulene/joal .