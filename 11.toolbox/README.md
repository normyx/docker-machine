# Create the tools box

## First create a container from ubuntu and run the shell to install packages
Insert all the volumes needed
```bash
source ~/env.sh
sudo docker run -v $DB_DUMP_FOLDER:/data -ti --rm ubuntu /bin/bash
```
Updates and add packages
```bash
apt update && apt install -y lsb-core sshpass zip curl ssh 
```
When done, connect to SSH to create creadential
```bash
ssh Mathieu@ns
```
Enter the password when asked
```bash
root@26c152da5250:/# ssh Mathieu@ns
The authenticity of host 'ns (192.168.0.11)' can't be established.
ED25519 key fingerprint is SHA256:FRCGiMTLBKmBYW42jDTe9rJjBDClgEWbA77jz1IViVU.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added 'ns' (ED25519) to the list of known hosts.
Mathieu@ns's password: 

Synology strongly advises you not to run commands as the root user, who has
the highest privileges on the system. Doing so may cause major damages
to the system. Please note that if you choose to proceed, all consequences are
at your own risk.
```
Whitout exiting, in another shell, commit the state in a new image
Find id of the container
```bash
docker ps 
```
```bash
CONTAINER ID   IMAGE                                COMMAND                  CREATED         STATUS                  PORTS                                                                                                                                                                             NAMES
26c152da5250   ubuntu                               "/bin/bash"              6 minutes ago   Up 6 minutes                                                                                                                                                                                              strange_dirac
```
Save the image
```bash
docker commit -p 26c152da5250 mgoulene/toolbox
``` 
Now you can use the image in a save state
```bash
docker run -ti -v $DB_DUMP_FOLDER:/data --rm mgoulene/toolbox ls /data 
```
copying the `dump.sql` to ssh
```bash
docker run -ti -v $DB_DUMP_FOLDER:/data --rm mgoulene/toolbox sshpass -p Racl3t98! scp /data/dump.sql Mathieu@ns:../../repository/mha/
```

# Add the dump scripts to cron

Edit the crontable
```bash
crontab -e
```
Add the lines
```
00 03 * * * /home/mathieu/docker-machine/11.toolbox/dump_myaccount.sh
01 03 * * * /home/mathieu/docker-machine/11.toolbox/dump_onedev.sh
```


