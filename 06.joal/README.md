# Installing joal
This installation will create a VPN via proton vpn (thanks to [gluetune](https://github.com/qdm12/gluetun)).
This is base on [joal(https://github.com/anthonyraymond/joal)]
Configuration `docker-compose.yalm` has been done thanks to [here](https://medium.com/linux-shots/put-a-docker-container-behind-vpn-fdc0e32c9ca5)
To run the docker 
```sh
docker compose up -d
```
To access the webapp, click [here](http://docker-machine:9100/joal/ui/#/) 
