# Installation
```bash
docker volume create portainer_data
docker pull portainer/portainer-ce:latest
docker run -d -p 9000:9000 --name=portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/docker_data/portainer_data portainer/portainer-ce:latest
```