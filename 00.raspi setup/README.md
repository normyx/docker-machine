# Installing Docker
## Source
[Installing Docker on the Raspberry Pi](https://pimylifeup.com/raspberry-pi-docker/)
## Steps
1. Install docker
```bash
curl -sSL https://get.docker.com | sh
```
2. Add user to docker group
```bash
sudo usermod -aG docker $USER
```
3. reboot
4. test Groups
```bash
groups
```
`docker` might be in the list
5. tests
 ```bash
docker run hello-world
```
# Installing Docker Compose
```bash
sudo curl -L https://github.com/docker/compose/releases/download/v2.25.0/docker-compose-`uname -s`-`uname -m` > docker-compose
sudo mv docker-compose /usr/bin/
sudo chown root: /usr/bin/docker-compose
sudo chmod +x /usr/bin/docker-compose
```

# Installing Samba
## Install Samba
```bash
sudo apt-get install samba samba-common-bin
```
## setup Samba
Edit the conf file
```bash
sudo nano /etc/samba/smb.conf
``` 
Add the configuration
```
[pishare]
   path = /home/mathieu
   writeable=Yes
   create mask=0777
   directory mask=0777
   public=no
```
Add password
```bash
sudo smbpasswd -a mathieu
``` 
Restart service
```bash
sudo systemctl restart smbd
``` 